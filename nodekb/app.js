const express=require('express');
const path=require('path');
const app=express();


// ładowanie lilnika widoków

app.set('views', path.join(__dirname,'views'));
app.set('view engine', 'pug');



app.get('/', function(req,res){

  let articles=[
      {
          id:1,
          title:'Article One',
          author:'Marcin Kula',
          body: 'This is article one'
      },
       {
          id:2,
          title:'Article Two',
          author:'Jan Kolwaski',
          body: 'This is article Two'
      },
       {
          id:1,
          title:'Article Three',
          author:'Marcin Kula',
          body: 'This is article Three'
      }
  ];


    res.render('index',{
        title: 'Articels',
        articles: articles
    });
});
app.get('/articles/add', function(req, res){
    res.render('add_article',{
        title: 'Add Artice'
    });
});



app.listen(3000, function(){
    console.log('Server started on port 3000.....')
});