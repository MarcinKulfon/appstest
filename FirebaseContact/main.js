// Initialize Firebase
  var config = {
    apiKey: "AIzaSyAd3yqKTwVMtojBEuTrZSrOpuxa8awVVAU",
    authDomain: "contactform-2f32b.firebaseapp.com",
    databaseURL: "https://contactform-2f32b.firebaseio.com",
    projectId: "contactform-2f32b",
    storageBucket: "contactform-2f32b.appspot.com",
    messagingSenderId: "942032316697"
  };
  firebase.initializeApp(config);

var messagesRef = firebase.database().ref('messages');

document.getElementById('contactForm').addEventListener('submit', submitForm);

function submitForm(e){

    e.preventDefault();

    

    // 
    var name=getInputVal('name');
    var company=getInputVal('company');
    var email=getInputVal('email');
    var phone=getInputVal('phone');
    var message=getInputVal('message');

    saveMessage(name, company, email, phone, message);
    
    document.querySelector('.alert').style.display = 'block';

    setTimeout(function(){
        document.querySelector('.alert').style.display='none';
    }, 3000);

    document.getElementById('contactForm').reset();
    


}
// funkcja, która odbiera wartości z formy

function getInputVal(id){

    return document.getElementById(id).value;

}

// zapisywanie wiadomości

function saveMessage(name, company,email,phone, message){
  
  var newMessageRef=messagesRef.push();

newMessageRef.set({
    name:name,
    company:company,
    email:email,
    phone:phone,
    message:message
});
}